import os, telebot, time

from utils import get_site_status
from threading import Thread
from datetime import datetime

BOT_TOKEN = os.environ.get('BOT_TOKEN')
BOT_ENV = os.environ.get('BOT_ENV')
BOT_VERSION = os.environ.get('BOT_VERSION')
BOT_TIMESTAMP = os.environ.get('BOT_TIMESTAMP')

site_list = ['https://olegen.ru', 'https://www.sapstore.com/solutions/99043/SAP-Crystal-Reports', 'https://store.sap.com', 'https://partner.store.sap.com','https://partnerbenefitscatalog.sap.com']
# site_list = ['https://olegen.ru', 'https://test.olegen.ru']
# site_list = ['https://olegen.ru']
admin_chat_id = 160459182
delay_time = 120
# delay_time = 5

bot = telebot.TeleBot(BOT_TOKEN)

@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
    text = "Howdy, how are you doing?\n\n \
This is web sites checker bot!\n\n \
Use:\n /help - Get help\n \
/check - Check status\n \
/sitelist - Show site list\n\n \
\
/autocheck\_start - Start autocheck\n \
/autocheck\_stop - Stop autocheck\n \
/autocheck\_status - Stop autocheck\n"
    # show current env
    if BOT_ENV != "prod" : text = text + \
"\nEnvironment: *"+BOT_ENV+"*\n"
    if BOT_VERSION != "" : text = text + \
"\nVersion: *"+BOT_VERSION+"*\n"
    if BOT_TIMESTAMP != "" : text = text + \
"Date: "+BOT_TIMESTAMP+"\n"
    bot.send_message(message.chat.id, text, parse_mode="Markdown")

@bot.message_handler(commands=['sitelist'])
def send_sitelist(message):
    bot.send_message(message.chat.id, '\n'.join(site_list), disable_web_page_preview=True)

@bot.message_handler(commands=['check'])
def send_sitestatus(message):
    for site in site_list:
        site_status = get_site_status(site)
        if site_status['code'] == 200 : emoji_prefix = "✅ "
        else : emoji_prefix = "❌ "
        text = emoji_prefix + site + "\n" + site_status['text'] + "\n"
        bot.send_message(message.chat.id, text, disable_web_page_preview=True)

@bot.message_handler(commands=['autocheck_stop'])
def send_autocheckstop(message):
    global stop_threads
    global t
    if t.is_alive() :
        stop_threads = True
        t.join()
        bot.send_message(message.chat.id, "❌ Autocheck stopped", disable_web_page_preview=True)
    else:
        bot.send_message(message.chat.id, "Autocheck was already stopped", disable_web_page_preview=True)

@bot.message_handler(commands=['autocheck_start'])
def send_autocheckstart(message):
    global stop_threads
    global t
    if not (t.is_alive()) :
        stop_threads = False
        t = Thread(target=site_status_checker, args =(lambda : stop_threads, ))
        t.start()
        bot.send_message(message.chat.id, "✅ Autocheck started", disable_web_page_preview=True)
    else:
        bot.send_message(message.chat.id, "Autocheck was already started", disable_web_page_preview=True)

@bot.message_handler(commands=['autocheck_status'])
def send_autocheckstatus(message):
    global stop_threads
    global t
    if t.is_alive() :
        bot.send_message(message.chat.id, "✅ Autocheck was started", disable_web_page_preview=True)
    else:
        bot.send_message(message.chat.id, "❌ Autocheck was stopped", disable_web_page_preview=True)

@bot.message_handler(func=lambda msg: True)
def echo_all(message):
    bot.reply_to(message, "Command not found")

def site_status_checker(stop):
    downtimes = []
    # create list of downtimes
    for site in site_list : downtimes.append({'site': site, 'start': "", 'period': "", 'message_id': 0, 'status': False})
    while True:
        for i, site in enumerate(site_list) :
            site_status = get_site_status(site)
            # site is up
            if site_status['code'] == 200 :
                # site is finally up after downtime
                if downtimes[i]['status'] :
                    emoji_prefix = "✅ "
                    text = emoji_prefix + site + "\n" + site_status['text'] + "\n"\
    "\nDowntime start: " + str(downtimes[i]['start']) + \
    "\nDowntime finish: " + site_status['time'] + "\n" + \
    "\nDowntime period: " + str(downtimes[i]['period']).split('.', 2)[0]
                    bot.send_message(admin_chat_id, text, disable_web_page_preview=True)
                    downtimes[i]['start'] = ""
                    downtimes[i]['period'] = ""
                    downtimes[i]['status'] = False
                    downtimes[i]['message_id'] = 0
                downtimes[i]['status'] = False
            else :
                # site is down for the first time
                if not (downtimes[i]['status']) : 
                    downtimes[i]['start'] = site_status['time']
                    downtimes[i]['status'] = True
                    downtimes[i]['period'] = datetime.now() - datetime.strptime(downtimes[i]['start'], "%d/%m/%Y %H:%M:%S")
                    emoji_prefix = "❌ "
                    text = emoji_prefix + site + "\n" + site_status['text'] + "\n"\
        "\nDowntime start: " + str(downtimes[i]['start']) + \
        "\nDowntime period: " + str(downtimes[i]['period']).split('.', 2)[0]
                    message = bot.send_message(admin_chat_id, text, disable_web_page_preview=True)
                    downtimes[i]['message_id'] = message.id
                # site is still down - editing current message
                else:
                    downtimes[i]['status'] = True
                    downtimes[i]['period'] = datetime.now() - datetime.strptime(downtimes[i]['start'], "%d/%m/%Y %H:%M:%S")
                    emoji_prefix = "❌ "
                    text = emoji_prefix + site + "\n" + site_status['text'] + "\n"\
        "\nDowntime start: " + str(downtimes[i]['start']) + \
        "\nDowntime period: " + str(downtimes[i]['period']).split('.', 2)[0]
                    bot.edit_message_text(text, admin_chat_id, downtimes[i]['message_id'], disable_web_page_preview=True)
        # for x in downtimes: 
        #     print(x)
        # print('\n')
        time.sleep(delay_time)
        if stop(): break

text = "✅ Bot started at " + datetime.now().strftime("%d/%m/%Y %H:%M:%S")
if BOT_VERSION != "" : text = text + \
"\nVersion: *"+BOT_VERSION+"*\n"
if BOT_TIMESTAMP != "" : text = text + \
"Date: "+BOT_TIMESTAMP+"\n"
bot.send_message(admin_chat_id, text, parse_mode="Markdown")

stop_threads = False
t = Thread(target=site_status_checker, args =(lambda : stop_threads, ))
t.start()
bot.infinity_polling()