from datetime import datetime
import requests

#
# Get site status_code or error message
# 
def get_site_status(url: str):
    site_status = dict()
    now = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    
    site_status['url'] = url
    site_status['time'] = now
    site_status['text'] = ""

    try:
        response = requests.get(url,timeout=5)
        site_status['code'] = response.status_code
        response.raise_for_status()
        
        # print (site_status['time'],site_status['url'] ,site_status['text'])
    except requests.exceptions.HTTPError as errh:
        status_message = "Http Error: " + str(errh)
        site_status['code'] = 0
        site_status['text'] = status_message
        
        # print (site_status['time'],site_status['url'] ,site_status['text'])
    except requests.exceptions.ConnectionError as errc:
        status_message = "Error Connecting: " + str(errc)
        site_status['code'] = 0
        site_status['text'] = status_message
        
        # print (site_status['time'],site_status['url'] ,site_status['text'])
    except requests.exceptions.Timeout as errt:
        status_message = "Timeout Error: " + str(errt)
        site_status['code'] = 0
        site_status['text'] = status_message
        
        # print (site_status['time'],site_status['url'] ,site_status['text'])
    except requests.exceptions.RequestException as err:
        status_message = "OOps: Something Else " + str(err)
        site_status['code'] = 0
        site_status['text'] = status_message

        # print (site_status['time'],site_status['url'] ,site_status['text'])
    return site_status